# NYC property sales Data Analysis

This project includes several notebooks for collection, merging, explorative analysis NYC property sales data. It expands the works of [NYC Property Sales on kaggle](https://www.kaggle.com/new-york-city/nyc-property-sales). Instead of just analysing one year of property sales, i build a dataset that includes all property sales listed [here](https://www1.nyc.gov/site/finance/taxes/property-rolling-sales-data.page).

The notebooks can be found in nyc_housing_data/notebooks.


Setup:
Python 3.8.10 was used for the development and Poetry as the dependency management tool.


Python isoliert installieren mit [pyenv](https://github.com/pyenv-win/pyenv-win):
```pyenv install 3.8.10```

[Poetry](https://python-poetry.org/) für das dependency management installieren, dann im Ordner:
```poetry env use $(pyenv which python)```

Poetry sollte dann ein virtuelles environment anlegen und die dependencies installieren.

Mit ´´´poetry shell´´´ kann dann das virtuelle Environment aktiviert werden.
