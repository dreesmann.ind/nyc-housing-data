import asyncio
import aiofiles
import httpx
from requests_html import HTMLSession
import pathlib

data_home = pathlib.Path("../data")
raw = data_home / "raw"
processed = data_home / "processed"


def get_nyc_housing_data_download_urls():

    nyc_open_data = (
        "https://www1.nyc.gov/site/finance/taxes/property-annualized-sales-update.page"
    )

    with HTMLSession() as session:
        resp = session.get(nyc_open_data)

    download_urls = [
        url
        for url in resp.html.absolute_links
        if (".xls" in url and not "citywide" in url and not "sales_prices" in url)
    ]

    # make sure only excel files included
    file_endings = [url.split(".")[-1] for url in download_urls]
    assert set(file_endings) == {"xls", "xlsx"}

    return download_urls


async def download_one_nyc_housing_data_xls(url):
    async with httpx.AsyncClient() as client:
        r = await client.get(url)

    async with aiofiles.open(raw / "nyc_housing_data" / url.split("/")[-1], "wb") as f:
        await f.write(r.content)


async def download_nyc_housing_data():
    urls = get_nyc_housing_data_download_urls()
    await asyncio.gather(*[download_one_nyc_housing_data_xls(url) for url in urls])
